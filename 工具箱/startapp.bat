call %~dp0\config.bat

:: 配置应用路径
set PATH_APP_UNSIGNED=%PATH_PROJECT%\build\outputs\hap\debug\phone\entry-debug-rich-unsigned.hap
set PATH_APP_SIGNED=%OUTPUT_DIR%\entry-debug-rich-signed.hap

:: 配置签名工具
set PATH_P7B=%OUTPUT_DIR%\SgnedReleaseProfileTemplate.p7b
set PATH_PEM=%PATH_SIGNCENTER%\certificates\OpenHarmonyApplication.pem
set PATH_P12=%PATH_SIGNCENTER%\key\OpenHarmony.p12
set PATH_SIGN_TOOL=%PATH_SIGNCENTER%\hapsigntool\hapsigntoolv2.jar

@REM hdc smode
@REM timeout /T 1
@REM hdc target mount
@REM timeout /T 1

:: 应用签名
java -jar %PATH_SIGN_TOOL% sign -mode localjks -privatekey "OpenHarmony Application Release" -inputFile %PATH_APP_UNSIGNED% -outputFile %PATH_APP_SIGNED% -keystore %PATH_P12% -keystorepasswd 123456 -keyaliaspasswd 123456 -signAlg SHA256withECDSA -profile "%PATH_P7B%" -certpath "%PATH_PEM%"

:: 烧写 fielio
@REM hdc file send %PATH_FILEIO% /system/lib/module/libfileio.z.so

:: 拉起应用
hdc_std uninstall %NAME_BUNDLE%
hdc_std app install -r %PATH_APP_SIGNED%
@REM hdc_std file send %PATH_APP_SIGNED% /data/temp.hap
@REM hdc_std shell chmod 777 /data/temp.hap
@REM hdc_std shell bm install -p /data/temp.hap
hdc_std shell aa start -a %NAME_ABILITY% -b %NAME_BUNDLE%
