:: 应用 bundle 名;
set NAME_BUNDLE=ohos.tutorial.application
:: 应用 ability 名
set NAME_ABILITY=ohos.tutorial.application.MainAbility

:: deveco studio 的安装位置
set PATH_DEVECO=C:\Program Files\Huawei\DevEco Studio 2.1.0.501

:: deveco 工程的位置
set PATH_PROJECT=C:\Users\mkl\DevEcoStudioProjects\MyApplication

::签名中心的位置（OpenHarmony\prebuilts\signcenter）
set PATH_SIGNCENTER=\\wsl$\Ubuntu-20.04\home\mkl\ohos\prebuilts\signcenter

:: fileio 库的位置
set PATH_FILEIO=D:\dev\OpenHarmony\out\ohos-arm-release\distributeddatamgr\distributedfilejs\libfileio.z.so

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: 下面这些看你心情修改
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:: 签名工具的密码
set passwd=123456 

:: 生成签名文件及 HAP 包的位置
set OUTPUT_DIR=D:\output

:: 签名文件的命名
set APP_NAME=application