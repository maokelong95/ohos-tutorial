call %~dp0\config.bat

set KEYTOOL=start "" "%PATH_DEVECO%\jbr\bin\keytool.exe"

set PATH_P12_OHOS=%PATH_SIGNCENTER%\key\OpenHarmony.p12
set PAHT_PROFILESIGNTOOL=%PATH_SIGNCENTER%\profilesigntool\provisionsigtool.jar
set PATH_TEMPLATE_JSON=%PATH_SIGNCENTER%\profilesigntool\UnsgnedReleasedProfileTemplate.json
set PATH_PEM=%PATH_SIGNCENTER%\certificates\OpenHarmonyProfileRelease.pem

:: 生成文件
set PATH_CSR=%OUTPUT_DIR%\%APP_NAME%.csr
set PATH_P12_TMP=%OUTPUT_DIR%\%APP_NAME%.p12
set PATH_CER=%OUTPUT_DIR%\%APP_NAME%.cer
set PATH_p7b=%OUTPUT_DIR%\SgnedReleaseProfileTemplate.p7b

mkdir %OUTPUT_DIR%

:: 生成 CSR 文件
%KEYTOOL% -genkeypair -alias "%APP_NAME%" -keyalg EC -sigalg SHA256withECDSA -dname "C=CN,O=HUAWEI,OU=HUAWEI IDE,CN=%APP_NAME%" -keystore %PATH_P12_TMP% -storetype pkcs12 -validity 9125 -storepass %passwd% -keypass %passwd%

timeout /T 1

%KEYTOOL% -certreq -alias "%APP_NAME%" -keystore %PATH_P12_TMP% -storepass %passwd% -keypass %passwd% -storetype pkcs12 -file %PATH_CSR% 

timeout /T 1

:: 签名
%KEYTOOL% -gencert -alias "OpenHarmony Application CA" -infile %PATH_CSR% -outfile %PATH_CER% -keystore %PATH_P12_OHOS% -sigalg SHA256withECDSA -storepass %passwd% -ext KeyUsage:"critical=digitalSignature" -validity 3650 -rfc

timeout /T 1

java -jar %PAHT_PROFILESIGNTOOL% sign --in %PATH_TEMPLATE_JSON% --out %PATH_p7b% --keystore %PATH_P12_OHOS% --storepass %passwd% --alias "OpenHarmony Application Profile Release" --sigAlg SHA256withECDSA --cert %PATH_PEM% --validity 365 --developer-id ohosdeveloper --bundle-name %NAME_BUNDLE% --distribution-certificate %PATH_CER%