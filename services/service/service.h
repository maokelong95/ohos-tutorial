/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "service_stub.h"

#include <mutex>

#include "i_service.h"
#include "iremote_stub.h"
#include "system_ability.h"

namespace OHOS {
namespace Tutorial {
class Service : public SystemAbility, public ServiceStub, protected NoCopyable {
    DECLARE_SYSTEM_ABILITY(Service);

public:
    explicit Service(int32_t saID, bool runOnCreate = true) : SystemAbility(saID, runOnCreate){};
    ~Service() = default;

    void OnStart() override;
    void OnStop() override;

    int32_t EchoServer(const std::string &echoStr) override;
    void DumpObj(const ComplexObject &obj) override;
    int32_t GetFd() override;

private:
    Service();
    static sptr<Service> instance_;
    static std::mutex instanceLock_;
};
} // namespace Tutorial
} // namespace OHOS