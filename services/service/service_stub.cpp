/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "service_stub.h"
#include "utils_log_wrapper.h"

namespace OHOS {
namespace Tutorial {
using namespace std;

ServiceStub::ServiceStub()
{
    opToInterfaceMap_[SERVICE_CMD_ECHO] = &ServiceStub::CmdEchoServer;
    opToInterfaceMap_[SERVICE_CMD_DUMPOBJ] = &ServiceStub::CmdDumpObj;
    opToInterfaceMap_[SERVICE_CMD_OUTFD] = &ServiceStub::CmdGetFd;
}

int32_t ServiceStub::OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option)
{
    LOGI("Begin to call procedure with code %{public}u", code);
    auto interfaceIndex = opToInterfaceMap_.find(code);
    if (interfaceIndex == opToInterfaceMap_.end() || !interfaceIndex->second) {
        LOGE("Cannot response request %d: unknown tranction", code);
        return IPCObjectStub::OnRemoteRequest(code, data, reply, option);
    }

    const std::u16string descriptor = ServiceStub::GetDescriptor();
    const std::u16string remoteDescriptor = data.ReadInterfaceToken();
    if (descriptor != remoteDescriptor) {
        LOGE("Check remote descriptor failed");
        return IPC_STUB_INVALID_DATA_ERR;
    }

    return (this->*(interfaceIndex->second))(data, reply);
}

int32_t ServiceStub::CmdEchoServer(MessageParcel &data, MessageParcel &reply)
{
    LOGI("Begin to dispatch cmd EchoServer");
    string echoStr = data.ReadString();
    int32_t strLen = EchoServer(echoStr);
    reply.WriteInt32(strLen);
    LOGI("EchoServer has recved str %{public}s with length %{public}d", echoStr.c_str(), strLen);

    return ERR_NONE;
}

int32_t ServiceStub::CmdDumpObj(MessageParcel &data, MessageParcel &reply)
{
    auto obj = ComplexObject::Unmarshalling(data);
    DumpObj(*obj);

    return ERR_NONE;
}
int32_t ServiceStub::CmdGetFd(MessageParcel &data, MessageParcel &reply)
{
    LOGI("Begin to dispatch Cmd GetFd");
    int fd = GetFd();
    auto ret = reply.WriteFileDescriptor(fd);
    if (!ret) {
        LOGI("ServiceStub::WriteFileDescriptor %{public}d", ret);
    }
    return ret;
}
} // namespace Tutorial
} // namespace OHOS