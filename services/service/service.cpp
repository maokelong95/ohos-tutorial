/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "service.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "system_ability_definition.h"
#include "utils_log_wrapper.h"

namespace OHOS {
namespace Tutorial {
using namespace std;

REGISTER_SYSTEM_ABILITY_BY_ID(Service, TUTORIAL_SA_ID, true);

void Service::OnStart()
{
    LOGI("Begin Service");
    bool res = SystemAbility::Publish(this);
    LOGI("End Service, res = %{public}d", res);
}

void Service::OnStop()
{
    LOGI("Done Service");
}

int32_t Service::EchoServer(const std::string &echoStr)
{
    LOGI("Service::EchoServer %{public}s", echoStr.c_str());
    return echoStr.length();
}

void Service::DumpObj(const ComplexObject &obj)
{
    LOGI("field1 = %{public}d, field2 = %{public}d", obj.field1_, obj.field2_);
}

int32_t Service::GetFd()
{
    int fd = open("//data/local/tmp/open_file.txt", O_RDWR | O_CREAT | O_APPEND, S_IRWXU);
    if (fd < 0) {
        LOGI("ServiceStub::CmdGeTtutorialFd %{public}d %{public}s", errno, strerror(errno));
    }
    return fd;
}

} // namespace Tutorial
} // namespace OHOS