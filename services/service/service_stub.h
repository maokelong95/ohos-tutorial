/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <map>

#include "i_service.h"
#include "iremote_stub.h"

namespace OHOS {
namespace Tutorial {
class ServiceStub : public IRemoteStub<IService> {
public:
    ServiceStub();
    ~ServiceStub() = default;
    int32_t OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option) override;

private:
    using ServiceInterface = int32_t (ServiceStub::*)(MessageParcel &data, MessageParcel &reply);
    std::map<uint32_t, ServiceInterface> opToInterfaceMap_;

    int32_t CmdEchoServer(MessageParcel &data, MessageParcel &reply);
    int32_t CmdDumpObj(MessageParcel &data, MessageParcel &reply);
    int32_t CmdGetFd(MessageParcel &data, MessageParcel &reply);
};
} // namespace Tutorial
} // namespace OHOS