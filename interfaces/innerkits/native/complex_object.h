/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "parcel.h"

namespace OHOS {
namespace Tutorial {
class ComplexObject final : public Parcelable {
public:
    ComplexObject() {}
    ComplexObject(int field1, int field2) : field1_(field1), field2_(field2) {}
    ~ComplexObject() {}

    int32_t field1_{0};
    int32_t field2_{0};

    bool Marshalling(Parcel &parcel) const override;
    static std::unique_ptr<ComplexObject> Unmarshalling(Parcel &parcel);
};
} // namespace Tutorial
} // namespace OHOS