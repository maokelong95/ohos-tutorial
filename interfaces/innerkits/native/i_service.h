/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <string>

#include "complex_object.h"
#include "iremote_broker.h"

namespace OHOS {
namespace Tutorial {
class IService : public IRemoteBroker {
public:
    enum {
        SERVICE_CMD_ECHO = 0,
        SERVICE_CMD_DUMPOBJ = 1,
        SERVICE_CMD_OUTFD = 2,
    };

    virtual int32_t EchoServer(const std::string &echoStr) = 0;
    virtual void DumpObj(const ComplexObject &obj) = 0;
    virtual int32_t GetFd() = 0;

    DECLARE_INTERFACE_DESCRIPTOR(u"OHOS.Tutorial.IService")
};
} // namespace Tutorial
} // namespace OHOS