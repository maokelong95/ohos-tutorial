#include "napi/native_api.h"
#include "napi/native_node_api.h"

namespace OHOS {
namespace Tutorial {
static napi_value Method(napi_env env, napi_callback_info info)
{
    napi_status status;
    napi_value world;
    status = napi_create_string_utf8(env, "world", 5, &world);
    return world;
}

#define DECLARE_NAPI_METHOD(name, func)         \
    {                                           \
        name, 0, func, 0, 0, 0, napi_default, 0 \
    }

static napi_value Init(napi_env env, napi_value exports)
{
    napi_status status;
    napi_property_descriptor desc = DECLARE_NAPI_METHOD("hello", Method);
    status = napi_define_properties(env, exports, 1, &desc);
    return exports;
}
} // namespace Tutorial
} // namespace OHOS

static napi_module demoModule = {
    .nm_version = 1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = OHOS::Tutorial::Init,
    .nm_modname = "hellojs",
    .nm_priv = ((void *)0),
    .reserved = {0},
};

extern "C" __attribute__((constructor)) void RegisterModule(void)
{
    napi_module_register(&demoModule);
}