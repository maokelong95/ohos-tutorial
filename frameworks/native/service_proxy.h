/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "i_service.h"
#include "iremote_proxy.h"

namespace OHOS {
namespace Tutorial {
class ServiceProxy : public IRemoteProxy<IService> {
public:
    explicit ServiceProxy(const sptr<IRemoteObject> &impl) : IRemoteProxy<IService>(impl) {}
    ~ServiceProxy() override {}

    int32_t EchoServer(const std::string &echoStr) override;
    void DumpObj(const ComplexObject &obj) override;
    int32_t GetFd() override;

private:
    static inline BrokerDelegator<ServiceProxy> delegator_;
};
} // namespace Tutorial
} // namespace OHOS