/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "complex_object.h"

#include <memory>

namespace OHOS {
namespace Tutorial {
using namespace std;

bool ComplexObject::Marshalling(Parcel &parcel) const
{
    parcel.WriteInt32(field1_);
    parcel.WriteInt32(field2_);
    return true;
}

unique_ptr<ComplexObject> ComplexObject::Unmarshalling(Parcel &parcel)
{
    auto obj = make_unique<ComplexObject>();
    obj->field2_ = parcel.ReadInt32();
    obj->field1_ = parcel.ReadInt32();
    return obj;
}

} // namespace Tutorial
} // namespace OHOS
