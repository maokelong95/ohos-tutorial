/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "service_proxy.h"
#include "utils_log_wrapper.h"

namespace OHOS {
namespace Tutorial {
using namespace std;

int32_t ServiceProxy::EchoServer(const string &echoStr)
{
    LOGI("Begin to echo %{public}s", echoStr.c_str());
    MessageParcel data;
    data.WriteInterfaceToken(GetDescriptor());
    data.WriteString(echoStr);

    MessageParcel reply;
    MessageOption option;
    Remote()->SendRequest(IService::SERVICE_CMD_ECHO, data, reply, option);

    return reply.ReadInt32();
}

void ServiceProxy::DumpObj(const ComplexObject &obj)
{
    MessageParcel data;
    data.WriteParcelable(&obj);

    MessageParcel reply;
    MessageOption option;
    Remote()->SendRequest(IService::SERVICE_CMD_DUMPOBJ, data, reply, option);
}

int32_t ServiceProxy::GetFd()
{
    LOGI("Begin to GetFd");
    MessageParcel data;
    data.WriteInterfaceToken(GetDescriptor());

    MessageParcel reply;
    MessageOption option;
    auto ret = Remote()->SendRequest(IService::SERVICE_CMD_OUTFD, data, reply, option);
    if (ret != NO_ERROR) {
        LOGE("Get GetFd SendRequest %{public}d", ret);
        return -1;
    }

    int fd = reply.ReadFileDescriptor();
    return fd;
}
} // namespace Tutorial
} // namespace OHOS