## 配置环境

### 获取源码

建议使用马明帅提供的一键脚本([点我跳转](https://gitee.com/landwind/openharmony_oneclick_env_init)）拉取代码。

### 首次编译

假设你正在使用 wsl，则可以试着在 PC 端新建一个脚本，如 `build.bat`，其内容如下：

```bat
ubuntu2004.exe run /home/mkl/repos-sep/openharmony_oneclick_env_init/OpenHarmony/build.sh --product-name rk3568  --ccache

pause
```

## 集成本套代码

### 添加部件

编辑 `productdefine/common/products/rk3568.json`，添加如下配置：

```
"filemanagement:ohos-tutorial":{},
```
### 添加服务 ID

编辑 `OpenHarmony/foundation/distributedschedule/samgr/interfaces/innerkits/samgr_proxy/include/system_ability_definition.h`，添加如下配置：

```
TUTORIAL_SA_ID                                   = 4999,
```

## 编译验证

### 验证 SA

```bat
ubuntu2004.exe run /home/mkl/repos-sep/openharmony_oneclick_env_init/OpenHarmony/build.sh --product-name rk3568  --ccache --build-target ohos-tutorial

pause
```

从前到后，依次作如下尝试：

1. `ps -A`
1. `bm dump -a`

### 验证 NAPI